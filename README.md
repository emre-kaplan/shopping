# Shopping Cart

A shopping cart website written in Symfony 5.2

## Installation

You need PHP 7+ and MySQL to run code. Firstly, you should create a database (e.g. `shopping`)
and run `doctrine:migrations:migrate`. Migrations will automatically create tables in db.