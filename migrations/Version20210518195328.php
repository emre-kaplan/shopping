<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210518195328 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE checkout RENAME INDEX idx_f5299398a76ed395 TO IDX_AF382D4EA76ED395');
        $this->addSql('ALTER TABLE product ADD deleted_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `checkout` RENAME INDEX idx_af382d4ea76ed395 TO IDX_F5299398A76ED395');
        $this->addSql('ALTER TABLE product DROP deleted_at');
    }
}
