<?php

namespace App\Controller;

use App\Entity\Checkout;
use App\Entity\Product;
use App\Form\ProductFormType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="get_admin_panel")
     */
    public function getPanel(Request $request)
    {
        $form = $this->createForm(ProductFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();
        }

        $products = $this->getDoctrine()->getRepository(Product::class)->findBy(['deletedAt' => null]);
        $orders = $this->getDoctrine()->getRepository(Checkout::class)->findAll();

        return $this->render('admin.html.twig', [
            'form' => $form->createView(), 'products' => $products, 'orders' => $orders
        ]);
    }

    /**
     * @Route("/products/{id}/delete", name="delete_product", methods={"DELETE"})
     * @param int $id
     * @return RedirectResponse
     */
    public function deleteProduct(int $id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        $product = $product->setDeletedAt(new DateTime()); // Soft delete it so orders can relate with it.

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        $this->addFlash('deleted', 'Product was successfully deleted.');

        return $this->redirectToRoute('get_admin_panel');
    }
}