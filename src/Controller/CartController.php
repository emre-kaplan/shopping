<?php

namespace App\Controller;

use App\Service\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    private $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @Route("/cart")
     */
    public function cartPage(Request $request)
    {
        $cartItems = $this->cartService->getCartFromSession();

        if ($request->getMethod() == 'POST') {
            try {
                $this->cartService->deleteItemFromCart($request->request->get('productId'));

                $success = true;
            } catch (\Exception $e) {
                $error = 'Item cannot be removed from cart.';
            }
        }

        return $this->render(
            'cart.html.twig', ['cartItems' => $cartItems, 'success' => $success ?? '', 'error' => $error ?? '']
        );
    }
}