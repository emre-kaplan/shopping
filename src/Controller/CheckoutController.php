<?php

namespace App\Controller;

use App\Entity\CartItem;
use App\Entity\Checkout;
use App\Entity\CheckoutProduct;
use App\Entity\Product;
use App\Entity\User;
use App\Service\CartService;
use App\Service\CheckoutService;
use App\Service\ProductService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CheckoutController extends AbstractController
{
    const NOT_ENOUGH_STOCK = 'There is not enough quantity on stock.';

    private $checkoutService;
    private $cartService;
    private $productService;
    private $entityManager;

    public function __construct(CheckoutService $checkoutService, CartService $cartService, ProductService $productService, EntityManagerInterface $entityManager)
    {
        $this->cartService = $cartService;
        $this->productService = $productService;
        $this->entityManager = $entityManager;
        $this->checkoutService = $checkoutService;
    }

    /**
     * @Route("/checkout")
     */
    public function checkoutPage(Request $request)
    {
        $cartItems = $this->cartService->getCartFromSession();

        if ($request->getMethod() == 'POST') {
            $user = $this->getDoctrine()->getRepository(User::class)->findByUsername($this->getUser()->getUsername());

            $checkOut = new Checkout();
            $checkOut = $checkOut
                ->setUser($user)
                ->setAddress($request->request->get('address'))
                ->setStatus('waiting')
                ->setDatetime(new DateTime());

            $this->entityManager->persist($checkOut);

            /** @var CartItem $cartItem */
            foreach ($cartItems as $cartItem) {
                // Check stock again.
                $isProductLeft = $this->productService->checkStock($cartItem->getProductId(), $cartItem->getQuantity());

                if (!$isProductLeft) {
                    $error = self::NOT_ENOUGH_STOCK;

                    break;
                }

                $product = $this->getDoctrine()->getRepository(Product::class)
                    ->find($cartItem->getProductId());

                $checkOutProduct = new CheckoutProduct();
                $checkOutProduct = $checkOutProduct
                    ->setCheckout($checkOut)
                    ->setProduct($product)
                    ->setQuantity($cartItem->getQuantity());

                $this->entityManager->persist($checkOutProduct);
            }

            $this->entityManager->flush();
        }

        return $this->render('checkout.html.twig', ['cartItems' => $cartItems, 'error' => $error ?? '']);
    }

    /**
     * @Route("/checkouts/{id}/approve", name="approve_checkout", methods={"POST"})
     */
    public function approveCheckout(int $id)
    {
        $this->checkoutService->approveCheckout($id);

        return $this->redirectToRoute('get_admin_panel');
    }

    /**
     * @Route("/orders", name="get_orders")
     */
    public function userCheckouts()
    {
        $orders = $this->checkoutService->getUserCheckouts($this->getUser()->getUsername());

        return $this->render('orders.html.twig', ['orders' => $orders]);
    }
}