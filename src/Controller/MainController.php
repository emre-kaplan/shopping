<?php

namespace App\Controller;

use App\Entity\Product;
use App\Factory\CartItemFactory;
use App\Service\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    private $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        if ($request->getMethod() == 'POST') {
            $cartItemFactory = new CartItemFactory();
            $cartItem = $cartItemFactory->createFrom($request);

            try {
                $this->cartService->addItem($cartItem);

                $success = true;
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }
        }

        return $this->render(
            'index.html.twig', ['products' => $products, 'success' => $success ?? '', 'error' => $error ?? '']
        );
    }
}