<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();

        $admin
            ->setUsername('admin')
            ->setPassword($this->passwordEncoder->encodePassword($admin, '123456789'))
            ->setRoles(['ROLE_ADMIN']); // Every user has ROLE_USER automatically.

        $manager->persist($admin);
        $manager->flush();
    }
}
