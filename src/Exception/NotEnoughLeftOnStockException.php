<?php

namespace App\Exception;

use RuntimeException;

class NotEnoughLeftOnStockException extends RuntimeException
{
    protected $message = 'There is not enough quantity on stock.';
}