<?php

namespace App\Factory;

use App\Entity\CartItem;
use Symfony\Component\HttpFoundation\Request;

class CartItemFactory
{
    public function createFrom(Request $request): CartItem
    {
        $cartItem = new CartItem();
        
        return $cartItem
            ->setProductId($request->request->get('id'))
            ->setQuantity($request->request->get('quantity'));
    }
}