<?php

namespace App\Service;

use App\Entity\CartItem;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService
{
    const TOO_LOW_STOCK = 'There is too low left on stock. Please specify a lesser value.';

    private $session;
    private $productRepository;

    public function __construct(SessionInterface $session, ProductRepository $productRepository)
    {
        $this->session = $session;
        $this->productRepository = $productRepository;
    }

    public function addItem(CartItem $cartItem)
    {
        $product = $this->productRepository->find($cartItem->getProductId());

        // Validate quantity.
        $quantity = $cartItem->getQuantity();
        $stock = $product->getStock();

        if ($quantity > $stock) {
            throw new \LogicException(self::TOO_LOW_STOCK);
        }

        // Calculate price.
        $price = $product->getPrice() * $quantity;

        $cartItem = $cartItem->setProductName($product->getName())->setPrice($price);

        $cartItems = $this->session->get('cartItems', []);

        /** @var CartItem $oldCartItem */
        if ($oldCartItem = $cartItems[$cartItem->getProductId()] ?? null) {
            $cartItem = $cartItem
                ->setQuantity($oldCartItem->getQuantity() + $quantity)
                ->setPrice($oldCartItem->getPrice() + $price);
        }

        $cartItems[$cartItem->getProductId()] = $cartItem;

        $this->session->set('cartItems', $cartItems);

        return $cartItem;
    }

    public function getCartFromSession()
    {
        return $this->session->get('cartItems', []);
    }

    public function deleteItemFromCart($productId)
    {
        $cartItems = $this->session->get('cartItems', []);

        unset($cartItems[$productId]);

        $this->session->set('cartItems', $cartItems);
    }
}