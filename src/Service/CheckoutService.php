<?php

namespace App\Service;

use App\Repository\CheckoutRepository;
use App\Repository\UserRepository;

class CheckoutService
{
    private $checkoutRepository;
    private $userRepository;

    public function __construct(CheckoutRepository $checkoutRepository, UserRepository $userRepository)
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->userRepository = $userRepository;
    }

    public function getUserCheckouts(string $username)
    {
        $user = $this->userRepository->findByUsername($username);

        return $this->checkoutRepository->findBy(['user' => $user]);
    }

    public function approveCheckout(int $id)
    {
        $checkOut = $this->checkoutRepository->find($id);
        $checkOut = $checkOut->setStatus('approved');

        $this->checkoutRepository->save($checkOut);

        return true;
    }
}