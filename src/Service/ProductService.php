<?php

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;

class ProductService
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function checkStock(int $productId, int $quantity)
    {
        $product = $this->productRepository->find($productId);

        return $product->getStock() >= $quantity;
    }

    public function deleteProduct(int $id)
    {
        $this->productRepository->delete($id);
    }
}